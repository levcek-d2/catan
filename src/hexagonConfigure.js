const hexagonConfigure = (
    size, 
    round = r => r
) => {
    const [ radius, side, xOffset, yOffset ] =
    [
        size / 2,
        size / 2,
        size / 2 - Math.sqrt(3) * size / 4,
        size / 4
    ].map(v => round?.(v) || v);
    return {
        size,
        side,
        netSize: size - 2 * xOffset,
        radius,
        xOffset,
        yOffset,
        d:  [
                [xOffset, yOffset],
                [side, 0],
                [2 * radius - xOffset, yOffset],
                [2 * radius - xOffset, yOffset + side],
                [side, 2 * side],
                [xOffset, yOffset + side]
            ]
            .map(coords => coords.map(round || (v => v)))
            .map(([x,y], i) => `${i ? "L" : "M"} ${x},${y}`)
            .concat(["Z"])
            .join(" "),
        attributes: {}
    };
};
export default hexagonConfigure;